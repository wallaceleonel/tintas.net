# Calculadora de tinta para pintura de salas

### Objetivo do Projeto
  Criar uma aplica��o que
  Permita calcular a quantidade de tinta necess�ria para pintar uma sala
  Tenha uma arquitetura que permita crescimento e futuras implementa��es
  Descri��o do projeto
  A aplica��o foi desenvolvida utilizando Programa��o Funcional.

### Funcionamento
  As paredes pode ter entre 1 a 15 metros de altura e largura
  As janelas possuem valor padr�o de 2,00 x 1,20 metros
  As portas possuem valor padr�o de 0,80 x 1,90 metros
  Cada litro de tinta � capaz de pintar 5 metros quadrados
  Piso e teto n�o s�o considerados
  O total da �rea de portas e janelas n�o pode exceder 50% da �rea de cada parede
  As paredes devem ter no m�nimo 30 cent�metros a mais de altura que portas e janelas, caso possuam alguma
  As paredes devem ser no m�nimo 30 cent�metros a mais de largura que a janela, caso possuam alguma

### Requisitos do Sistema
  C#
  .NET core
  Entity Framework Core
  Vistual Studio

### Como rodar localmente
  Fa�a o clone do projeto
   ````sh
   git clone https://gitlab.com/wallaceleonel/tintas.net.git
   ```` 
  Entre na pasta do projeto
  Acesse o arquivo .sln
  Rode localmente ou inicie o programa em container usando Docker.